OBJ_DIR := obj
OBJ_DUMMY := $(shell mkdir -p $(OBJ_DIR))
SRC_DIR := src
MANPAGE := startmutt.1
MANPAGE_COMPRESSED := $(MANPAGE).gz

SRC_FILES := \
	main.c

DEP_FILES := $(SRC_FILES:%.c=$(OBJ_DIR)/%.d)
OBJ_FILES := $(SRC_FILES:%.c=$(OBJ_DIR)/%.o)
SRC_FILES := $(SRC_FILES:%=$(SRC_DIR)/%)

EXECUTABLE := startmutt

CFLAGS ?=-g -c -Wall -Wextra -Wformat-security -pedantic -std=c11

CC := gcc

.PHONY: all
all: $(EXECUTABLE) $(MANPAGE_COMPRESSED)

$(EXECUTABLE): $(OBJ_FILES)
	$(CC) $(LDFLAGS) $(OBJ_FILES) -o $(EXECUTABLE)

-include $(DEP_FILES)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -o $@ $<

$(MANPAGE_COMPRESSED):
	gzip -c $(MANPAGE) > $(MANPAGE_COMPRESSED)

.PHONY: clean
clean: 
	rm -f $(OBJ_FILES)
	rm -f $(DEP_FILES)
	rm -f $(EXECUTABLE)
	rm -f $(MANPAGE_COMPRESSED)

.PHONY:
install: $(EXECUTABLE) $(MANPAGE_COMPRESSED)
	cp $(EXECUTABLE) /usr/local/bin/$(EXECUTABLE)
	cp $(MANPAGE_COMPRESSED) /usr/local/share/man/man1/$(MANPAGE_COMPRESSED)

.PHONY:
uninstall:
	rm -f /usr/local/bin/$(EXECUTABLE)
	rm -f /usr/local/share/man/man1/$(MANPAGE_COMPRESSED)
