/* main.c
 * Entry point, argument parsing, and display logic.
 */

#define _GNU_SOURCE

#include <ctype.h>
#include <dirent.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define ENTRY_CHUNK_SIZE 4096

static void display_entries(char* item_names[], size_t num_items) {
	for(size_t i = 0; i < num_items; i++) {
		printf("  [%zd] %s\n", i + 1, item_names[i]);
	}
}

static bool string_startswith(const char* s1, const char* s2) {
	if(strlen(s1) < strlen(s2))
		return false;
	for(size_t pos = 0; pos < strlen(s2); pos++) {
		if(s1[pos] != s2[pos])
			return false;
	}
	return true;
}

static int entry_is_profile(const struct dirent* entry) {
	if(string_startswith(entry->d_name, ".xinitrc#"))
		return 1;
	return 0;
}

static size_t get_entries(char*** names) {
	size_t count = 0;
	const char* home_path = getenv("HOME");
	if(!home_path)
		errx(1, "no HOME variable");

	struct dirent** entries;
	int num_entries = scandir(home_path, &entries, entry_is_profile, alphasort);
	if(num_entries < 0 )
		err(1, "scandir");
	*names = calloc(num_entries, sizeof(char*));
	if(!*names)
		err(1, "calloc");

	for(int i = 0; i < num_entries; i++) {
		struct dirent* entry = entries[i];
		char* name;
		if(!asprintf(&name, "%s", entry->d_name + strlen(".xinitrc#")))
			err(1, "asprintf");
		(*names)[count++] = name;
		free(entry);
	}
	free(entries);
	return count;
}

static void copy_file(const char* source, const char* target) {
	// TODO: Is a remove call actually necessary? We'll be overwriting it.
	if(remove(target) < 0) {
		// Ignore the ENOENT error, since we'll be overwriting target anyway.
		if(errno != ENOENT)
			err(1, "remove");
	}
	char buf[4096];
	ssize_t count_read;

	int fd_source = open(source, O_RDONLY);
	if(fd_source < 0)
		err(1, "open");
	int fd_target = open(target, O_WRONLY | O_CREAT | O_EXCL, 0644);
	if(fd_target < 0) {
		close(fd_source);
		err(1, "open");
	}

	while((count_read = read(fd_source, buf, sizeof(buf))) > 0) {
		ssize_t count_written = 0;
		char* write_ptr = buf;
		while(count_written < count_read) {
			ssize_t tmp_written = write(fd_target, write_ptr, count_read);
			if(tmp_written < 0) {
				close(fd_source);
				close(fd_target);
				err(1, "write");
			}
			count_written += tmp_written;
			write_ptr += tmp_written;
		}
	}

	// TODO: We can probably ignore these close errors. Maybe warn about them?
	if(close(fd_source) < 0)
		err(1, "close");
	if(close(fd_target) < 0)
		err(1, "close");
}

static unsigned int interactive_select(size_t entry_count) {
	int selection = -1; 
	do {
		printf("Select one to load: ");
		selection = fgetc(stdin);
		if(isdigit(selection))
			selection = selection - '0' - 1;
		if(selection == '\n')
			continue;
		char c;
		while((c = fgetc(stdin)) != '\n' && c != EOF);
	}
	while((unsigned int)selection > entry_count - 1 || selection < 0);
	return selection;
}

int main(int argc, char* argv[]) {
	bool interactive = true;
	bool quiet = false;
	bool spawn_startx = true;
	char* selection_name = NULL;
	int selection = 0;
	int c;
	opterr = 0;
	while((c = getopt(argc, argv, "nqxs:")) != -1) {
		switch(c) {
			case 'n':
				interactive = false;
				break;
			case 'q':
				quiet = true;
				break;
			case 's':
				interactive = false;
				selection_name = optarg;
				break;
			case 'x':
				spawn_startx = false;
				break;
			case '?':
				if(optopt == 's')
					errx(1, "option -%c requires an argument", optopt);
				else if(isprint(optopt))
					errx(1, "unknown option -%c", optopt);
				else
					errx(1, "unknown option character \\x%x", optopt);
			default:
				return 1;
		}
	}
	if(!interactive && !selection_name)
		errx(1, "must either use interactive mode or specify selection name");

	char** names;
	size_t entry_count = get_entries(&names);
	if(entry_count <= 0)
		errx(1, "no entries found - see manual page");
	if(interactive) {
		if(!quiet)
			printf("Found %zd entries:\n", entry_count);
		display_entries(names, entry_count);
		selection = interactive_select(entry_count);
	}
	else if(selection_name) {
		bool found = false;
		for(selection = 0; (unsigned int)selection < entry_count; selection++) {
			if(strcmp(selection_name, names[selection]) == 0) {
				found = true;
				break;
			}
		}
		if(!found)
			errx(1, "could not find entry %s", selection_name);
	}

	const char* home_path = getenv("HOME");
	if(!home_path)
		errx(1, "no HOME variable");
	char* source_path;
	if(!asprintf(&source_path, "%s/%s%s", home_path, ".xinitrc#",
	             names[selection]))
		err(1, "asprintf");
	char* xinitrc_path;
	if(!asprintf(&xinitrc_path, "%s/%s", home_path, ".xinitrc"))
		err(1, "asprintf");
	copy_file(source_path, xinitrc_path);
	if(interactive && !quiet)
		printf("Copied %s to %s\n", source_path, xinitrc_path);

	free(source_path);
	free(xinitrc_path);
	for(size_t i = 0; i < entry_count; i++) {
		free(names[i]);
	}
	free(names);

	if(spawn_startx) {
		if(interactive && !quiet)
			printf("Starting startx\n");
		pid_t mypid = fork();
		if(mypid < 0)
			err(1, "fork");
		if(mypid != 0) {
			if(execl("/usr/bin/startx", "/usr/bin/startx") < 0)
				err(1, "exec");
		}
	}
	else if(interactive && !quiet)
		printf("Not executing startx\n");

	return 0;
}
